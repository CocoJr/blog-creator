$(document).ready(function(){
	//-----------definition des variables-----------//
	var marg_left = 0;
	var nbr_img = 0;
	var current_img = 1;
	var slider = $("#slider");
	var transition = "horizontal";

	//-----------count img + affichage bullet + affichage miniatures-----------//
	slider.find("img").each(function(){
		nbr_img += 1;
		$("#bullets").append('<div class="bullet" id="'+nbr_img+'"></div>');
		src = $(this).attr("src");
		$("#mini").append('<img class="miniature" src="'+src+'" data-mini="'+nbr_img+'">');
	});
	$("#1").addClass("active");
	$(".miniature[data-mini=1]").addClass("active");
	$("#slider").css("width",""+nbr_img*480+"");

	//-----------fonction de déplacement des img-----------//
	function move(){
		
		if (current_img > nbr_img || current_img < 1) {
			marg_left = 0;
			current_img = 1;
			$("#1").addClass("active");
			$(".miniature[data-mini=1]").addClass("active");
		};
		transitions();
	};

	//-----------fonction pour mettre en activ les bulles quand clic bulle-----------//
	// function bullets(bullet){
	// 	id_bullet = bullet.attr("id");
	// 	current_img = id_bullet;
	// 	if (id_bullet) {
	// 		$(".bullet").removeClass("active");
	// 		bullet.addClass("active");
	// 	}
	// };

	//-----------fonction pour mettre en activ les miniatures quand clic miniature-----------//
	function miniature(miniature){
		id_minia = miniature.attr("data-mini");
		current_img = id_minia;
		if (id_minia) {
			$(".miniature").removeClass("active");
			miniature.addClass("active");
		}
	};

	//-----------fonction pour mettre en activ les bulles quand clic fleche-----------//
	function fleche_activ_bull(){
		$(".bullet").removeClass("active");
		$("#"+current_img).addClass("active");
	};

	//-----------fonction pour mettre en activ les miniatures quand clic fleche-----------//
	function fleche_activ_miniature(){
		$(".miniature").removeClass("active");
		$(".miniature[data-mini="+current_img+"]").addClass("active");
	};

	//-----------fonction pour changer d'animation-----------//
	function transitions(){
		// if (transition == "horizontal"){
		// 	slider.animate({"margin-left": marg_left},300, "linear");
		// }
		// else if(transition == "fondu"){
			slider.fadeOut(222);
			slider.animate({"margin-left": marg_left},300, "linear");
			slider.fadeIn(222);
		// }
		// else if(transition == "vertical"){
		// 	slider.animate({"margin-top": -350},0, "linear");
		// 	slider.animate({"margin-left": marg_left},300, "linear");
		// 	slider.animate({"margin-top": 0},300, "linear");
		// }
	};

	//-----------navigation par clic suiv-----------//
	// $("#suiv").click(function(){
	// 	current_img +=1;
	// 	marg_left = marg_left-560;
	// 	fleche_activ_bull();
	// 	fleche_activ_miniature();
	// 	move();
	// });

	//-----------navigation par clic prec-----------//
	// $("#prec").click(function(){
	// 	current_img -=1;
	// 	marg_left = marg_left+560;
	// 	fleche_activ_bull();
	// 	fleche_activ_miniature();
	// 	move();
	// });

	//-----------navigation par clic bullet-----------//
	// $(".bullet").click(function(){
	// 	bullets($(this));
	// 	marg_left = (id_bullet -1)* -560;
	// 	fleche_activ_miniature();
	// 	move();
	// });

	//-----------navigation par clic miniature-----------//
	$(".miniature").click(function(){
		miniature($(this));
		marg_left = (id_minia -1)* -480;
		fleche_activ_bull();
		move();
	});

	//-----------Choix option select-----------//
	// $("#select").change(function(){
	// 	transition = $(this).val();
	// });

});//fin du doc.ready