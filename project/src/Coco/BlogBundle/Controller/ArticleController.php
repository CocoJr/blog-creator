<?php

namespace Coco\BlogBundle\Controller;

use Coco\BlogBundle\Entity\Article;
use Coco\BlogBundle\Entity\Blog;
use Coco\BlogBundle\Entity\Category;
use Coco\BlogBundle\Form\Type\ArticleType;
use Coco\BlogBundle\Form\Type\BlogType;
use Coco\BlogBundle\Form\Type\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

class ArticleController extends Controller
{

    /**
     * List article
     *
     * @Route("/blog/administrer/{blog_id}/tous-les-articles/{page}", name="administrate_blog_list_article")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function listAction($blog_id, $page)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);

        if (empty($blog) or $blog->getAuthor() != $this->getUser()) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $paginator = $this->get('knp_paginator');

        $pagination = $em->getRepository('CocoBlogBundle:Article')->paginate($paginator, $page, $blog_id);

        return array(
            'blog' => $blog,
            'pagination' => $pagination,
        );
    }

    /**
     * Create article
     *
     * @Route("/blog/administrer/{blog_id}/creer-un-article", name="administrate_blog_create_article")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function createAction($blog_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);

        if (empty($blog) or $blog->getAuthor() != $this->getUser()) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $article = new Article();
        $article->setBlog($blog);
        $form = $this->createForm(new ArticleType(), $article, array('blog_id' => $blog_id))
            ->add('submit', 'submit', array('label' => 'Créer l\'article'));
        $form->handleRequest($this->get('request'));

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'L\'article à bien été créé');
            return $this->redirectToRoute('administrate_blog', array('blog_id' => $blog->getId()));
        }

        return array(
            'blog' => $blog,
            'form' => $form->createView(),
        );
    }

    /**
     * Delete article
     *
     * @Route("/blog/administrer/{blog_id}/supprimer-l-article/{article_id}", name="administrate_blog_delete_article")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function deleteAction($blog_id, $article_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);
        $article = $em->getRepository('CocoBlogBundle:Article')->find($article_id);

        if (empty($blog) or empty($article) or $blog->getAuthor() != $this->getUser() or $article->getBlog() != $blog) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $em->remove($article);
        $em->flush();
        $this->get('session')->getFlashBag()->add('notice', 'L\'article à bien été supprimé.');
        return $this->redirectToRoute('administrate_blog', array('blog_id' => $blog->getId()));
    }

    /**
     * Update article
     *
     * @Route("/blog/administrer/{blog_id}/modifier-l-article/{article_id}", name="administrate_blog_update_article")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function updateAction($blog_id, $article_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);
        $article = $em->getRepository('CocoBlogBundle:Article')->find($article_id);

        if (empty($blog) or empty($article) or $blog->getAuthor() != $this->getUser() or $article->getBlog() != $blog) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $form = $this->createForm(new ArticleType(), $article, array('blog_id' => $blog_id))
            ->add('submit', 'submit', array('label' => 'Modifier l\'article'));
        $form->handleRequest($this->get('request'));

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setUpdatedAt(new \DateTime());
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'L\'article à bien été modifié');
            return $this->redirectToRoute('administrate_blog', array('blog_id' => $blog->getId()));
        }

        return array(
            'blog' => $blog,
            'form' => $form->createView(),
        );
    }

}
