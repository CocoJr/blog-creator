<?php

namespace Coco\BlogBundle\Controller;

use Coco\BlogBundle\Entity\Article;
use Coco\BlogBundle\Entity\Blog;
use Coco\BlogBundle\Entity\Category;
use Coco\BlogBundle\Entity\Comment;
use Coco\BlogBundle\Form\Type\ArticleType;
use Coco\BlogBundle\Form\Type\BlogType;
use Coco\BlogBundle\Form\Type\CategoryType;
use Coco\BlogBundle\Form\Type\CommentType;
use Doctrine\Common\Util\Debug;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class BlogViewerController
 * @package Coco\BlogBundle\Controller
 *
 * @Route(
 *     path = "/",
 *     host = "{subdomain}.blog-creator.prod"
 * )
 */
class BlogViewerController extends Controller
{

    /**
     * Show a blog homepage.
     *
     * @Route("/", name="show_blog_homepage")
     * @Template()
     */
    public function homeAction(Request $request)
    {
        $currentHost = $request->getHttpHost();
        $baseHost = $this->container->getParameter('base_host');
        $subdomain = str_replace('.'.$baseHost, '', $currentHost);

        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->findOneBySlug($subdomain);

        if (empty($blog)) {
            $this->get('session')->getFlashBag()->add('error', 'Le blog n\'existe pas ou plus.');
            return $this->redirectToRoute('home');
        }

        return array(
            'blog' => $blog,
        );
    }

    /**
     * Show an article.
     *
     * @Route("/lire-l-article/{article_id}/", name="show_blog_article")
     * @Template()
     */
    public function showAction($article_id, Request $request)
    {
        $currentHost = $request->getHttpHost();
        $baseHost = $this->container->getParameter('base_host');
        $subdomain = str_replace('.'.$baseHost, '', $currentHost);

        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->findOneBySlug($subdomain);
        $article = $em->getRepository('CocoBlogBundle:Article')->find($article_id);

        if (empty($blog) OR empty($article) OR $article->getBlog() != $blog) {
            if (empty($blog)) {
                $this->get('session')->getFlashBag()->add('error', 'Le blog n\'existe pas ou plus.');
                return $this->redirectToRoute('home');
            } else {
                $this->get('session')->getFlashBag()->add('error', 'L\'article n\'existe pas ou plus.');
                return $this->redirectTo('http://'.$blog->getSlug().'.blog-creator.prod');
            }
        }

        $form_create_comment = $this->createForm(new CommentType())
        ->add('submit', 'submit', array('attr' => array('class' => 'btn btn-primary'), 'label' => 'Créer'));

        return array(
            'blog' => $blog,
            'form_create_comment' => $form_create_comment->createView(),
            'article' => $article,
        );
    }

    /**
     * Create comment
     *
     * @Route("/blog/administrer/{article_id}/creer-un-commentaire", name="show_blog_create_comment")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function createAction($article_id)
    {
        $em = $this->getDoctrine()->getManager();
        $article= $em->getRepository('CocoBlogBundle:Article')->find($article_id);

        if (empty($article)) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $comment = new Comment();
        $comment->setArticle($article);
        $comment->setAuthor($this->getUser());
        $form = $this->createForm(new CommentType(), $comment)
            ->add('submit', 'submit', array('label' => 'Créer le commentaire'));
        $form->handleRequest($this->get('request'));

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($comment);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'Le commentaire à bien été créé');
            return $this->redirectToRoute('show_blog_article', array('subdomain' => $article->getBlog()->getSlug(), 'article_id' => $article->getId()));
        }

        return array(
            'article' => $article,
            'form' => $form->createView(),
        );
    }
}
