<?php

namespace Coco\BlogBundle\Controller;

use Coco\BlogBundle\Entity\Article;
use Coco\BlogBundle\Entity\Blog;
use Coco\BlogBundle\Entity\Category;
use Coco\BlogBundle\Form\Type\ArticleType;
use Coco\BlogBundle\Form\Type\BlogType;
use Coco\BlogBundle\Form\Type\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Class BlogController
 * @package Coco\BlogBundle\Controller
 *
 * @Route(
 *     path = "/",
 *     host = "blog-creator.prod"
 * )
 */
class BlogController extends Controller
{

    /**
     * Create a blog.
     *
     * @Route("/blog/creer-un-blog", name="create_blog")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function createAction()
    {
        $blog = new Blog();
        $blog->setAuthor($this->getUser());
        $form = $this->createForm(new BlogType(), $blog);
        $form->handleRequest($this->get('request'));
        $em = $this->getDoctrine()->getEntityManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($blog);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'Le blog à bien été créé');
            /** @TODO Modifier la redirection */
            return $this->redirectToRoute('fos_user_profile_show');
        }

        return array(
            'form' => $form->createView(),
        );
    }
    
    /**
     * Administrate
     * 
     * @Route("/blog/administrer/{blog_id}", name="administrate_blog")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function administrateAction($blog_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);

        if (empty($blog) or $blog->getAuthor() != $this->getUser()) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $form_create_article = $this->createForm(new ArticleType(), null, array('blog_id' => $blog_id))
            ->add('submit', 'submit', array('label' => 'Créer l\'article'));
        $form_create_category = $this->createForm(new CategoryType())
            ->add('submit', 'submit', array('label' => 'Créer la catégorie'));

        $last_comments = $em->getRepository('CocoBlogBundle:Comment')->lastComments($blog->getId());

        return array(
            'blog' => $blog,
            'form_create_article' => $form_create_article->createView(),
            'form_create_category' => $form_create_category->createView(),
            'last_comments' => $last_comments,
        );
    }

    /**
     * Delete blog
     *
     * @Route("/blog/administrer/supprimer-le-blog/{blog_id}", name="administrate_delete_blog")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function deleteAction($blog_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);

        if (empty($blog) or $blog->getAuthor() != $this->getUser()) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $em->remove($blog);
        $em->flush();
        $this->get('session')->getFlashBag()->add('notice', 'Le blog à bien été supprimé.');
        return $this->redirectToRoute('fos_user_profile_show');
    }
}
