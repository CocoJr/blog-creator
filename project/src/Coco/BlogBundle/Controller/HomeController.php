<?php

namespace Coco\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class HomeController extends Controller
{
    /**
     * Homepage.
     *
     * @Route("/", name="home")
     */
    public function homeAction()
    {
        return $this->render('CocoBlogBundle:Home:home.html.twig');
    }
}
