<?php

namespace Coco\BlogBundle\Controller;

use Coco\BlogBundle\Entity\Article;
use Coco\BlogBundle\Entity\Blog;
use Coco\BlogBundle\Entity\Category;
use Coco\BlogBundle\Form\Type\ArticleType;
use Coco\BlogBundle\Form\Type\BlogType;
use Coco\BlogBundle\Form\Type\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;

class CategoryController extends Controller
{

    /**
     * Create category
     *
     * @Route("/blog/administrer/{blog_id}/creer-un-category", name="administrate_blog_create_category")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function createAction($blog_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);

        if (empty($blog) or $blog->getAuthor() != $this->getUser()) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $category = new Category();
        $category->setBlog($blog);
        $options['nb_categories'] = count($blog->getCategories());
        $form = $this->createForm(new CategoryType(), $category, $options)
            ->add('submit', 'submit', array('label' => 'Créer la catégorie'));
        $form->handleRequest($this->get('request'));

        if ($options['nb_categories'] > 9) {
            $this->get('session')->getFlashBag()->add('error', 'Vous ne pouvez pas ajouter plus de 10 catégories par blog.');
            return $this->redirectToRoute('administrate_blog', array('blog_id' => $blog->getId()));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($category);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'La categorie à bien été créé');
            return $this->redirectToRoute('administrate_blog', array('blog_id' => $blog->getId()));
        }

        return array(
            'blog' => $blog,
            'form' => $form->createView(),
        );
    }

    /**
     * Delete category
     *
     * @Route("/blog/administrer/{blog_id}/supprimer-la-category/{category_id}", name="administrate_blog_delete_category")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function deleteAction($blog_id, $category_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);
        $category = $em->getRepository('CocoBlogBundle:Category')->find($category_id);

        if (empty($blog) or empty($category) or $blog->getAuthor() != $this->getUser() or $category->getBlog() != $blog) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $em->remove($category);
        $em->flush();
        $this->get('session')->getFlashBag()->add('notice', 'La catégorie à bien été supprimé.');
        return $this->redirectToRoute('administrate_blog', array('blog_id' => $blog->getId()));
    }

    /**
     * Update category
     *
     * @Route("/blog/administrer/{blog_id}/modifier-la-categorie/{category_id}", name="administrate_blog_update_category")
     * @Secure(roles="ROLE_USER")
     * @Template()
     */
    public function updateAction($blog_id, $category_id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('CocoBlogBundle:Blog')->find($blog_id);
        $category = $em->getRepository('CocoBlogBundle:Category')->find($category_id);

        if (empty($blog) or empty($category) or $blog->getAuthor() != $this->getUser() or $category->getBlog() != $blog) {
            $this->get('session')->getFlashBag()->add('error', 'Une erreur est survenu');
            return $this->redirectToRoute('fos_user_profile_show');
        }

        $form = $this->createForm(new CategoryType(), $category)
            ->add('submit', 'submit', array('label' => 'Modifier la catégorie'));
        $form->handleRequest($this->get('request'));

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'La categorie à bien été modifié');
            return $this->redirectToRoute('administrate_blog', array('blog_id' => $blog->getId()));
        }

        return array(
            'blog' => $blog,
            'form' => $form->createView(),
        );
    }

}
