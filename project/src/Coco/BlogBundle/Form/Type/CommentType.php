<?php
namespace Coco\BlogBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content', 'textarea', array('label' => 'Commentaire', 'required' => true));

        $validator = function(FormEvent $event) {
            $form = $event->getForm();
            $size = \strlen($form->get('content')->getData());
            if($form->get('content')->getData() == null || $size < 100 || $size > 1000) {
                $form->get('content')->addError(new FormError('Le commentaire doit contenir au moins 100 caractères et au maximum 1000.'));
            }
        };

        $builder->addEventListener(FormEvents::BIND, $validator);
    }

    public function getName()
    {
        return 'comment';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coco\BlogBundle\Entity\Comment',
        ));
    }
}