<?php
namespace Coco\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', array('label' => 'Nom du blog'));
        $builder->add('description', 'textarea', array('label' => 'Description'));
        $builder->add('submit', 'submit', array('label' => 'Créer'));
    }

    public function getName()
    {
        return 'blog';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coco\BlogBundle\Entity\Blog',
        ));
    }
}