<?php
namespace Coco\BlogBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text', array('label' => 'Titre de l\'article'));
        $builder->add('content', 'textarea', array('attr' => array('class' => 'tinymce', 'data-theme' => 'advanced'), 'label' => 'Billet', 'required' => true));
        $builder->add('category', 'entity', array('class' => 'CocoBlogBundle:Category', 'property' => 'name', 'label' => 'Catégorie', 'query_builder' => function(EntityRepository $er) use($options) {
            return $er->createQueryBuilder('CocoBlogBundle:Category')
                ->select('c')
                ->from('CocoBlogBundle:Category', 'c')
                ->join('c.blog', 'b')
                ->where('b.id = :id')
                ->setParameter('id', $options['blog_id']);
        },));

        $validator = function(FormEvent $event) {
            $form = $event->getForm();
            $size = \strlen($form->get('content')->getData());
            if($form->get('content')->getData() == null || $size < 100) {
                $form->get('content')->addError(new FormError('Le billet doit contenir au moins 100 caractères.'));
            }
        };

        $builder->addEventListener(FormEvents::BIND, $validator);
    }

    public function getName()
    {
        return 'article';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coco\BlogBundle\Entity\Article',
            'blog_id' => null,
        ));
    }
}