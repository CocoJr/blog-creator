<?php
namespace Coco\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(isset($options['nb_categories'])) {
            $validator = function(FormEvent $event) use($options) {
                $form = $event->getForm();
                if($options['nb_categories'] > 9) {
                    $form->addError(new FormError('Vous ne pouvez pas ajouter plus de 10 catégories'));
                }
            };

            $builder->addEventListener(FormEvents::BIND, $validator);
        }
        $builder->add('name', 'text', array('label' => 'Nom de la catégorie'));
    }

    public function getName()
    {
        return 'category';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Coco\BlogBundle\Entity\Category',
            'nb_categories' => null,
        ));
    }
}