<?php

namespace Coco\BlogBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * Blog
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Coco\BlogBundle\Entity\BlogRepository")
 */
class Blog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=110)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /*
     * ----------------
     *     RELATION
     * ----------------
     */

    /**
     * @ORM\ManyToOne(targetEntity="Coco\UserBundle\Entity\User", inversedBy="blogs")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * @ORM\OneToMany(targetEntity="Coco\BlogBundle\Entity\Category", mappedBy="blog")
     */
    protected $categories;

    /**
     * @ORM\OneToMany(targetEntity="Coco\BlogBundle\Entity\Article", mappedBy="blog")
     */
    protected $articles;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Blog
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Blog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Blog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Blog
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user_visitor = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime;
        $this->updatedAt = new \DateTime;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Blog
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add user_visitor
     *
     * @param \Coco\UserBundle\Entity\User $userVisitor
     * @return Blog
     */
    public function addUserVisitor(\Coco\UserBundle\Entity\User $userVisitor)
    {
        $this->user_visitor[] = $userVisitor;

        return $this;
    }

    /**
     * Remove user_visitor
     *
     * @param \Coco\UserBundle\Entity\User $userVisitor
     */
    public function removeUserVisitor(\Coco\UserBundle\Entity\User $userVisitor)
    {
        $this->user_visitor->removeElement($userVisitor);
    }

    /**
     * Get user_visitor
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserVisitor()
    {
        return $this->user_visitor;
    }

    /**
     * Set author
     *
     * @param \Coco\UserBundle\Entity\User $author
     * @return Blog
     */
    public function setAuthor(\Coco\UserBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Coco\UserBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
