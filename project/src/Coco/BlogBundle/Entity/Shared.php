<?php

namespace Coco\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shared
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Coco\BlogBundle\Entity\SharedRepository")
 */
class Shared
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /*
     * ----------------
     *     RELATION
     * ----------------
     */

    /**
     * @ORM\ManyToOne(targetEntity="Coco\BlogBundle\Entity\Article", inversedBy="shared")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    protected $article;

    /**
     * @ORM\ManyToOne(targetEntity="Coco\BlogBundle\Entity\Friend", inversedBy="shared_articles")
     * @ORM\JoinColumn(name="friend_id", referencedColumnName="id")
     */
    protected $friend;

    /**
     * Constructor
     *
     * Create default value
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set article
     *
     * @param \Coco\BlogBundle\Entity\Article $article
     * @return Shared
     */
    public function setArticle(\Coco\BlogBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Coco\BlogBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set friend
     *
     * @param \Coco\BlogBundle\Entity\Friend $friend
     * @return Shared
     */
    public function setFriend(\Coco\BlogBundle\Entity\Friend $friend = null)
    {
        $this->friend = $friend;

        return $this;
    }

    /**
     * Get friend
     *
     * @return \Coco\BlogBundle\Entity\Friend 
     */
    public function getFriend()
    {
        return $this->friend;
    }
}
