<?php

namespace Coco\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SharedRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SharedRepository extends EntityRepository
{
}
