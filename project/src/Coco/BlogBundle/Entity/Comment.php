<?php

namespace Coco\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Coco\BlogBundle\Entity\CommentRepository")
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validate_at", type="datetime", nullable=true)
     */
    private $validateAt;

    /*
     * ----------------
     *     RELATION
     * ----------------
     */

    /**
     * @ORM\ManyToOne(targetEntity="Coco\BlogBundle\Entity\Article", inversedBy="comments")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    protected $article;

    /**
     * @ORM\ManyToOne(targetEntity="Coco\UserBundle\Entity\User", inversedBy="comments")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * Constructor
     *
     * Create default value
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Comment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set validateAt
     *
     * @param \DateTime $validateAt
     * @return Comment
     */
    public function setValidateAt($validateAt)
    {
        $this->validateAt = $validateAt;

        return $this;
    }

    /**
     * Get validateAt
     *
     * @return \DateTime 
     */
    public function getValidateAt()
    {
        return $this->validateAt;
    }

    /**
     * Set article
     *
     * @param \Coco\BlogBundle\Entity\Article $article
     * @return Comment
     */
    public function setArticle(\Coco\BlogBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Coco\BlogBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set author
     *
     * @param \Coco\UserBundle\Entity\User $author
     * @return Comment
     */
    public function setAuthor(\Coco\UserBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Coco\UserBundle\Entity\User 
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
