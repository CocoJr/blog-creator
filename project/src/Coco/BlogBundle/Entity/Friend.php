<?php

namespace Coco\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Friend
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Coco\BlogBundle\Entity\FriendRepository")
 */
class Friend
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accepted_at", type="datetime", nullable=true)
     */
    private $acceptedAt;

    /*
     * ----------------
     *     RELATION
     * ----------------
     */

    /**
     * @ORM\ManyToOne(targetEntity="Coco\UserBundle\Entity\User", inversedBy="friends_invited")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    protected $sender;

    /**
     * @ORM\ManyToOne(targetEntity="Coco\UserBundle\Entity\User", inversedBy="friends_asked")
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id")
     */
    protected $receiver;

    /**
     * @ORM\OneToMany(targetEntity="Coco\BlogBundle\Entity\Shared", mappedBy="friend", cascade={"persist", "remove"})
     */
    protected $shared_articles;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accepted
     *
     * @param boolean $accepted
     * @return Friend
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * Get accepted
     *
     * @return boolean 
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Friend
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set acceptedAt
     *
     * @param \DateTime $acceptedAt
     * @return Friend
     */
    public function setAcceptedAt($acceptedAt)
    {
        $this->acceptedAt = $acceptedAt;

        return $this;
    }

    /**
     * Get acceptedAt
     *
     * @return \DateTime 
     */
    public function getAcceptedAt()
    {
        return $this->acceptedAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->shared_articles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * Set sender
     *
     * @param \Coco\UserBundle\Entity\User $sender
     * @return Friend
     */
    public function setSender(\Coco\UserBundle\Entity\User $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \Coco\UserBundle\Entity\User 
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param \Coco\UserBundle\Entity\User $receiver
     * @return Friend
     */
    public function setReceiver(\Coco\UserBundle\Entity\User $receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \Coco\UserBundle\Entity\User 
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Add shared_articles
     *
     * @param \Coco\BlogBundle\Entity\Shared $sharedArticles
     * @return Friend
     */
    public function addSharedArticle(\Coco\BlogBundle\Entity\Shared $sharedArticles)
    {
        $this->shared_articles[] = $sharedArticles;

        return $this;
    }

    /**
     * Remove shared_articles
     *
     * @param \Coco\BlogBundle\Entity\Shared $sharedArticles
     */
    public function removeSharedArticle(\Coco\BlogBundle\Entity\Shared $sharedArticles)
    {
        $this->shared_articles->removeElement($sharedArticles);
    }

    /**
     * Get shared_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSharedArticles()
    {
        return $this->shared_articles;
    }
}
