<?php

namespace Coco\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CocoUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
