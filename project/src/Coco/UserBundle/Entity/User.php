<?php

namespace Coco\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="Coco\UserBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\Image(
     *     maxSize="2M",
     *     mimeTypes={"image/png","image/jpg","image/jpeg"},
     *     mimeTypesMessage="Choisissez une image JPG/PNG valide."
     * )
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imagePath;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /*
     * ----------------
     *     RELATION
     * ----------------
     */

    /**
     * @ORM\OneToMany(targetEntity="Coco\BlogBundle\Entity\Friend", mappedBy="sender", cascade={"persist", "remove"})
     */
    protected $friends_invited;

    /**
     * @ORM\OneToMany(targetEntity="Coco\BlogBundle\Entity\Friend", mappedBy="receiver", cascade={"persist", "remove"})
     */
    protected $friends_asked;

    /**
     * @ORM\OneToMany(targetEntity="Coco\BlogBundle\Entity\Blog", mappedBy="author", cascade={"persist", "remove"})
     */
    protected $blogs;

    /**
     * @ORM\OneToMany(targetEntity="Coco\BlogBundle\Entity\Comment", mappedBy="author", cascade={"persist", "remove"})
     */
    protected $comments;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function setImage(File $file = null)
    {
        $this->image = $file;
    }

    public function getImage()
    {
        return $this->image;
    }


    /*
     * Upload
     */
    public function getAbsolutePath()
    {
        return null === $this->imagePath ? null : $this->getUploadRootDir().'/'.$this->imagePath;
    }

    public function getWebPath()
    {
        return null === $this->imagePath ? null : $this->getUploadDir().'/'.$this->imagePath;
    }

    protected function getUploadRootDir()
    {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // on se débarrasse de « __DIR__ » afin de ne pas avoir de problème lorsqu'on affiche
        // le document/image dans la vue.
        return 'uploads/users/profile';
    }

    public function upload()
    {
        if (null === $this->image) {
            return;
        }
        $this->removeUpload();
        $name = sha1(uniqid(mt_rand(), true)).'.'.$this->image->guessExtension();
        $this->imagePath = $name;
        $this->image->move($this->getUploadRootDir(), $name);
        $this->image = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }


    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return User
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string 
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Add blogs
     *
     * @param \Coco\BlogBundle\Entity\Blog $blogs
     * @return User
     */
    public function addBlog(\Coco\BlogBundle\Entity\Blog $blogs)
    {
        $this->blogs[] = $blogs;

        return $this;
    }

    /**
     * Remove blogs
     *
     * @param \Coco\BlogBundle\Entity\Blog $blogs
     */
    public function removeBlog(\Coco\BlogBundle\Entity\Blog $blogs)
    {
        $this->blogs->removeElement($blogs);
    }

    /**
     * Get blogs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBlogs()
    {
        return $this->blogs;
    }

    /**
     * Add friends_invited
     *
     * @param \Coco\BlogBundle\Entity\Friend $friendsInvited
     * @return User
     */
    public function addFriendsInvited(\Coco\BlogBundle\Entity\Friend $friendsInvited)
    {
        $this->friends_invited[] = $friendsInvited;

        return $this;
    }

    /**
     * Remove friends_invited
     *
     * @param \Coco\BlogBundle\Entity\Friend $friendsInvited
     */
    public function removeFriendsInvited(\Coco\BlogBundle\Entity\Friend $friendsInvited)
    {
        $this->friends_invited->removeElement($friendsInvited);
    }

    /**
     * Get friends_invited
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFriendsInvited()
    {
        return $this->friends_invited;
    }

    /**
     * Add friends_asked
     *
     * @param \Coco\BlogBundle\Entity\Friend $friendsAsked
     * @return User
     */
    public function addFriendsAsked(\Coco\BlogBundle\Entity\Friend $friendsAsked)
    {
        $this->friends_asked[] = $friendsAsked;

        return $this;
    }

    /**
     * Remove friends_asked
     *
     * @param \Coco\BlogBundle\Entity\Friend $friendsAsked
     */
    public function removeFriendsAsked(\Coco\BlogBundle\Entity\Friend $friendsAsked)
    {
        $this->friends_asked->removeElement($friendsAsked);
    }

    /**
     * Get friends_asked
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFriendsAsked()
    {
        return $this->friends_asked;
    }

    /**
     * Add comments
     *
     * @param \Coco\BlogBundle\Entity\Comment $comments
     * @return User
     */
    public function addComment(\Coco\BlogBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Coco\BlogBundle\Entity\Comment $comments
     */
    public function removeComment(\Coco\BlogBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }
}
